<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'vincewp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';q*+G3OMjxdhH`;u5a{}RiMUKsEu8Q0gDnPlNQUT51p!Lt!DlJ8`o`Nk_=yixNy$');
define('SECURE_AUTH_KEY',  '>abN.iTc/QI&e?!:D+e E_~+(a)t=D8@qK0-o1!ag9lH>Ic_ySl)F7?s:8W]|[(W');
define('LOGGED_IN_KEY',    '-s)CsU<?]`!Iz:yZ:Uy8`=-3>U;|hH5IAQ?z^oTE?(K9>[ONUtde.1~%)yh+=UOA');
define('NONCE_KEY',        'A.P3y55ci7?AYuk{V`0|&N?H3|6yk=6e|J=@Qr@EvOn?X9=2ZBL)xit%@2GK]fkR');
define('AUTH_SALT',        '|ROh8:&&FGYu;>NK4R=Uhv|-i40Wg}O!gmti+KMNQ!9F1<goMa/|V+{^nMlB[{Ve');
define('SECURE_AUTH_SALT', '-x~~]sRFUK]q@,c0PjdUNeE(~=|KJb@]xB/v4$Qp|%3f?ja:,[xeEY/rQeMJ?d4h');
define('LOGGED_IN_SALT',   '*iZY]Pe~:rhbaEb;8LDQ?>`;Vla;UM?-NC|PrB*r|$/|/Ij*9[p@04+N6P+1D!x=');
define('NONCE_SALT',       'g=H80rG b|3jR)q3&N=/JQ!1#)bo]O|5t/CX!UR!isZozche~RGOazUPy2~=S50B');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
