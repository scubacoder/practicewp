
<footer class="site-footer">

  <!-- footer widgets -->
  <div class="footer-widgets clearfix">

  <?php if (is_active_sidebar('footer1' )) : ?>
    <div class="footer-widget-area">
      <?php dynamic_sidebar('footer1' );?>
    </div>
  <?php endif; ?>

  <?php if (is_active_sidebar('footer2' )) : ?>
    <div class="footer-widget-area">
      <?php dynamic_sidebar('footer2' );?>
    </div>
  <?php endif; ?>

  <?php if (is_active_sidebar('footer3' )) : ?>
    <div class="footer-widget-area">
      <?php dynamic_sidebar('footer3' );?>
    </div>
  <?php endif; ?>

  <?php if (is_active_sidebar('footer4' )) : ?>
    <div class="footer-widget-area">
      <?php dynamic_sidebar('footer4' );?>
    </div>
  <?php endif; ?>
    
  </div>

  <nav class="site-nav">
      <?php    /**
      * Displays a navigation menu
      * @param array $args Arguments
      */
      $args = array(
        'theme_location' => 'footer',
        // 'menu' => '',
        // 'container' => 'div',
        // 'container_class' => 'menu-{menu-slug}-container',
        // 'container_id' => '',
        // 'menu_class' => 'menu',
        // 'menu_id' => '',
        // 'echo' => true,
        // 'fallback_cb' => 'wp_page_menu',
        // 'before' => '',
        // 'after' => '',
        // 'link_before' => '',
        // 'link_after' => '',
        // 'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
        // 'depth' => 0,
        // 'walker' => ''
      );

      wp_nav_menu( $args );?>
    </nav>

  <p><?php bloginfo('name' );?> - &copy; <?php echo date('Y');?></p>
</footer>

</div> <!-- container -->

</body>
</html>