<?php
/**
 * Plugin Name: Reviews Plugin
 * Plugin URI: https://gitlab.com/scubacoder/practicewp
 * Description: A plugin for creating and displaying reviews
 * Author: Vincent Aycardo
 * Author URI: https://gitlab.com/scubacoder/practicewp
 * Version: 1.0
 * License: GPLv2
 */

/**
* Registers a new post type
* @uses $wp_post_types Inserts new post type object into the list
*
* @param string  Post type key, must not exceed 20 characters
* @param array|string  See optional args description above.
* @return object|WP_Error the registered post type object, or an error object
*/
function my_reviews_post() {

  $singular = "Review";
  $plural = "Reviews";

  $labels = array(
    "name"                => __( "$plural Name", "text-domain" ),
    "singular_name"       => __( "$singular Name", "text-domain" ),
    "add_new"             => _x( "Add New $singular", "text-domain", "text-domain" ),
    "add_new_item"        => __( "Add New $singular", "text-domain" ),
    "edit_item"           => __( "Edit $singular", "text-domain" ),
    "new_item"            => __( "New $singular", "text-domain" ),
    "view_item"           => __( "View $singular", "text-domain" ),
    "search_items"        => __( "Search $plural", "text-domain" ),
    "not_found"           => __( "No $plural Name found", "text-domain" ),
    "not_found_in_trash"  => __( "No $plural Name found in Trash", "text-domain" ),
    "parent_item_colon"   => __( "Parent $singular Name:", "text-domain" ),
    "menu_name"           => __( "$plural", "text-domain" ),
  );

  $args = array(
    'labels'                   => $labels,
    'hierarchical'        => false,
    'description'         => 'Reviews post type',
    'taxonomies'          => array('category', 'post_tag'),
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => 'dashicons-star-half',
    'show_in_nav_menus'   => true,
    'publicly_queryable'  => true,
    'exclude_from_search' => false,
    'has_archive'         => true,
    'query_var'           => true,
    'can_export'          => true,
    'rewrite'             => array('slug' => 'reivews'),
    'capability_type'     => 'post',
    'supports'            => array(
      'title', 'editor', 'author', 'thumbnail',
      'excerpt','custom-fields', 'trackbacks', 'comments',
      'revisions', 'page-attributes', 'post-formats'
      )
  );

  register_post_type( 'reviews', $args );
}

add_action( 'init', 'my_reviews_post' );