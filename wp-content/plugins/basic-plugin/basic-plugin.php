<?php
/**
 * Plugin Name: Basic Plugin
 * Plugin URI: https://gitlab.com/scubacoder/practicewp
 * Description: A plugin for creating and displaying job opportunities.
 * Author: Vincent Aycardo
 * Author URI: https://gitlab.com/scubacoder/practicewp
 * Version: 1.0
 * License: GPLv2
 */

// remove dashboard news
function dwwp_remove_dashboard_widget() {
  remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
}
// add_action( $tag, $function_to_add, $priority, $accepted_args );
add_action('wp_dashboard_setup', 'dwwp_remove_dashboard_widget' );

// add google analytics
function dwwp_add_google_link() {
  global $wp_admin_bar;
  $args = array(
    'id' => 'google_analytics',
    'title' => 'Google Analytics',
    'href' => 'http://google.com/analytics'
    );
  $wp_admin_bar->add_menu($args);
  
}
add_action( 'wp_before_admin_bar_render', 'dwwp_add_google_link');