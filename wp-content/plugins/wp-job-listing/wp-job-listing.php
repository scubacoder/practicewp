<?php
/**
 * Plugin Name: WP Job Plugin
 * Plugin URI: https://gitlab.com/scubacoder/practicewp
 * Description: This plugin allow you to add a simple job listing section to your WordPress website.
 * Author: Vincent Aycardo
 * Author URI: https://gitlab.com/scubacoder/practicewp
 * Version: 1.0
 * License: GPLv2
 */

// Exit if accessed directly
if (!defined('ABSPATH')) {
  exit();
}

// load files
require( plugin_dir_path(__FILE__ ) . 'wp-job-cpt.php');  // custom post text
// require( plugin_dir_path(__FILE__ ) . 'wp-job-render-admin.php');
require( plugin_dir_path(__FILE__ ) . 'wp-job-fields.php');
require( plugin_dir_path(__FILE__ ) . 'wp-job-settings.php');
require( plugin_dir_path(__FILE__ ) . 'wp-job-shortcode.php');

function dwwp_admin_enqueue_scripts(){
  global $pagenow, $typenow;

  if ($typenow == 'job') {
    wp_enqueue_style( 'dwwp-admin-css', plugins_url('css/admin-jobs.css', __FILE__));
  }

  if (($pagenow == 'post.php' || $pagenow == 'post-new.php') && $typenow == 'job') {
    
    wp_enqueue_script( 'dwwp-jobs-js', plugins_url('js/admin-jobs.js', __FILE__), array('jquery', 'jquery-ui-datepicker'), '20151012', true);
    wp_enqueue_style( 'jquery-style', 'https://code.jquery.com/ui/1.10.4/themes/cupertino/jquery-ui.css');
    wp_enqueue_script( 'dwwp-custom-quicktags', plugins_url('js/dwwp-quicktags.js', __FILE__), array('quicktags'), '20151012', true);
  }

  if ($pagenow == "edit.php" && $typenow == 'job') {
    wp_enqueue_script( 'reorder-js', plugins_url('js/reorder.js', __FILE__), array('jquery', 'jquery-ui-sortable'), '20151013', true);
    wp_localize_script( 'reorder-js', 'WP_JOB_LISTING', array(
      'security' => wp_create_nonce('wp-job-order' ),
      'success' => 'Job sort order has been saved.',
      'failure' => 'There was an error saving the sort order, or you do not have permission.'
      ) );
  }
}

add_action( 'admin_enqueue_scripts', 'dwwp_admin_enqueue_scripts' ); 
