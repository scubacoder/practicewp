<?php


function dwwp_register_post_type() {

  $singular = "Job Listing";
  $plural = "Job Listings";

  $labels = array(
    "name"                => __( "$plural", "text-domain" ),
    "singular_name"       => __( "$singular", "text-domain" ),
    "add_new"             => _x( "Add New $singular", "text-domain", "text-domain" ),
    "add_new_item"        => __( "Add New $singular", "text-domain" ),
    "edit_item"           => __( "Edit $singular", "text-domain" ),
    "new_item"            => __( "New $singular", "text-domain" ),
    "view_item"           => __( "View $singular", "text-domain" ),
    "search_items"        => __( "Search $plural", "text-domain" ),
    "not_found"           => __( "No $plural found", "text-domain" ),
    "not_found_in_trash"  => __( "No $plural found in Trash", "text-domain" ),
    "parent_item_colon"   => __( "Parent $singular:", "text-domain" ),
    "menu_name"           => __( "$plural", "text-domain" ),
  );

  $args = array(
    'labels'                   => $labels,
    'hierarchical'        => false,
    'description'         => 'description',
    'taxonomies'          => array(),
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 6,
    'menu_icon'           => 'dashicons-businessman',
    'show_in_nav_menus'   => true, 
    'publicly_queryable'  => true,
    'exclude_from_search' => false,
    'has_archive'         => true,
    'query_var'           => true,
    'can_export'          => true,
    'rewrite'             => array(
      'slug' => 'jobs',
      'with_front' => true,
      'pages' => true,
      'feeds' => false
      ),
    'capability_type'     => 'post',
    'supports'            => array(
      'title'
      )
  );
  register_post_type( 'job', $args );
}
add_action( 'init', 'dwwp_register_post_type');

// taxonomy
function dwwp_register_taxonomy() {
  
  $plural = "Locations";
  $singular = "Location";

  $labels = array(
    "name"          => _x( "$plural", "Taxonomy $plural", "text-domain" ),
    "singular_name"      => _x( "$singular", "Taxonomy $singular", "text-domain" ),
    "search_items"      => __( "Search $plural", "text-domain" ),
    "popular_items"      => __( "Popular $plural", "text-domain" ),
    "all_items"        => __( "All $plural", "text-domain" ),
    "parent_item"      => __( "Parent $singular", "text-domain" ),
    "parent_item_colon"    => __( "Parent $singular", "text-domain" ),
    "edit_item"        => __( "Edit $singular", "text-domain" ),
    "update_item"      => __( "Update $singular", "text-domain" ),
    "add_new_item"      => __( "Add New $singular", "text-domain" ),
    "new_item_name"      => __( "New $singular Name", "text-domain" ),
    "add_or_remove_items"  => __( "Add or remove $plural", "text-domain" ),
    "choose_from_most_used"  => __( "Choose from most used text-domain", "text-domain" ),
    "menu_name"        => __( "$singular", "text-domain" ),
  );

  $args = array(
    'labels'            => $labels,
    'public'            => true,
    // 'show_in_nav_menus' => true,
    'show_admin_column' => true,
    'hierarchical'      => true,
    // 'show_tagcloud'     => true,
    'show_ui'           => true,
    'query_var'         => true,
    'rewrite'           => true,
    'query_var'         => true,
    // 'capabilities'      => array(),
  );

  register_taxonomy( 'location', 'job', $args );
}
add_action( 'init', 'dwwp_register_taxonomy');