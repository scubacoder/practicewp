<?php

// submenu
function dwwp_add_submenu_page() {
  add_submenu_page( 
    'edit.php?post_type=job', 
    'Reorder Jobs', 
    'Reorder Jobs', 
    'manage_options', 
    'reorder_jobs', 
    'reorder_admin_jobs_callback' );
}
add_action( 'admin_menu', 'dwwp_add_submenu_page');

function reorder_admin_jobs_callback() {
    
    /**
     * The WordPress Query class.
     * @link http://codex.wordpress.org/Function_Reference/WP_Query
     *
     */
    $args = array(
      
      //Type & Status Parameters
      'post_type'   => 'job',
      'post_status' => 'publish',
      
      //Order & Orderby Parameters
      'order'               => 'ASC',
      'orderby'             => 'menu_order',
      
      // //Pagination Parameters
      'posts_per_page'         => 10,
      
      //Parameters relating to caching
      'no_found_rows'          => true,
      // 'cache_results'          => true,
      'update_post_term_cache' => false,
      // 'update_post_meta_cache' => true,
      
    );
  
  $job_listing = new WP_Query( $args );
  ?>

  <div id="job-sort" class="wrap">
  <div id="icon-job-admin" class="icon32"></div>
  <h2>
    <?php _e( 'Sort Job Positions', 'wp-job-listing' );?>
  <img src="<?php echo esc_url(admin_url().'/images/loading.gif');?>" alt="" id="loading-animation">
  </h2>

  <?php if ($job_listing->have_posts()) : ?>
  <p><?php _e( '<b>Note:</b> this only affects the jobs listed using the shortcode functions.', 'wp-job-listing' );?></p>
  <ul id="custom-type-list">
    <?php while($job_listing->have_posts()) : $job_listing->the_post(); ?>
      <li id="<?php esc_attr( the_id() ); ?>"><?php esc_html( the_title() ); ?></li>
    <?php endwhile; ?>
  </ul>
<?php else: ?>
  <p><?php _e( 'no jobs to sort', 'wp-job-listing' );?></p>
  <?php endif; ?>

  </div>
  
  <?php
  
}

// function to save ajax
function dwwp_save_reorder(){
  // check nonce
  if (!check_ajax_referer( 'wp-job-order', 'security' )){
    return wp_send_json_error('Invalid Nonce' );
  }

  // check credentials of current user
  if (! current_user_can('manage_options')) {
    return wp_send_json_error('You are not allowed to do this.' );
  }

  $order = $_POST['order'];
  $counter = 0;

  foreach ($order as $item_id) {
    $post = array(
      'ID' => intval($item_id),
      'menu_order' => $counter,
      );
    wp_update_post( $post );
    $counter++;
  }

  wp_send_json_success('Post Saved.' );
}
add_action( 'wp_ajax_save_sort', 'dwwp_save_reorder');