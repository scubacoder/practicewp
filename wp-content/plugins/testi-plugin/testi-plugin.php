<?php
/**
 * Plugin Name: Testi Plugin
 * Plugin URI: https://gitlab.com/scubacoder/practicewp
 * Description: A plugin for creating and displaying testimonials
 * Author: Vincent Aycardo
 * Author URI: https://gitlab.com/scubacoder/practicewp
 * Version: 1.0
 * License: GPLv2
 */

/**
* Registers a new post type
* @uses $wp_post_types Inserts new post type object into the list
*
* @param string  Post type key, must not exceed 20 characters
* @param array|string  See optional args description above.
* @return object|WP_Error the registered post type object, or an error object
*/
function my_testi_post() {

  $singular = "Testi";
  $plural = "Testis";

  $labels = array(
    "name"                => __( "$plural Name", "text-domain" ),
    "singular_name"       => __( "$singular Name", "text-domain" ),
    "add_new"             => _x( "Add New $singular", "text-domain", "text-domain" ),
    "add_new_item"        => __( "Add New $singular", "text-domain" ),
    "edit_item"           => __( "Edit $singular", "text-domain" ),
    "new_item"            => __( "New $singular", "text-domain" ),
    "view_item"           => __( "View $singular", "text-domain" ),
    "search_items"        => __( "Search $plural", "text-domain" ),
    "not_found"           => __( "No $plural Name found", "text-domain" ),
    "not_found_in_trash"  => __( "No $plural Name found in Trash", "text-domain" ),
    "parent_item_colon"   => __( "Parent $singular Name:", "text-domain" ),
    "menu_name"           => __( "$plural", "text-domain" ),
  );

  $args = array(
    'labels'                   => $labels,
    'hierarchical'        => false,
    'description'         => 'Testi post type',
    'taxonomies'          => array('category', 'post_tag'),
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => 'dashicons-admin-comments',
    'show_in_nav_menus'   => true,
    'publicly_queryable'  => true,
    'exclude_from_search' => false,
    'has_archive'         => true,
    'query_var'           => true,
    'can_export'          => true,
    'rewrite'             => array('slug' => 'reivews'),
    'capability_type'     => 'post',
    'supports'            => array(
      'title', 'editor', 'author', 'thumbnail',
      'excerpt','custom-fields', 'trackbacks', 'comments',
      'revisions', 'page-attributes', 'post-formats'
      )
  );

  register_post_type( 'testi', $args );
}

add_action( 'init', 'my_testi_post' );


/**
 * Create a taxonomy
 *
 * @uses  Inserts new taxonomy object into the list
 * @uses  Adds query vars
 *
 * @param string  Name of taxonomy object
 * @param array|string  Name of the object type for the taxonomy object.
 * @param array|string  Taxonomy arguments
 * @return null|WP_Error WP_Error if errors, otherwise null.
 */
function my_custom_taxonomies() {
  $singular = "Product type";
  $plural = "Product type";

  $labels = array(
    "name"          => _x( "$plural", "Taxonomy plural name", "text-domain" ),
    "singular_name"      => _x( "$singular", "Taxonomy singular name", "text-domain" ),
    "search_items"      => __( "Search $plural", "text-domain" ),
    "popular_items"      => __( "Popular $plural", "text-domain" ),
    "all_items"        => __( "All $plural", "text-domain" ),
    "parent_item"      => __( "Parent $singular", "text-domain" ),
    "parent_item_colon"    => __( "Parent $singular", "text-domain" ),
    "edit_item"        => __( "Edit $singular", "text-domain" ),
    "update_item"      => __( "Update $singular", "text-domain" ),
    "add_new_item"      => __( "Add New $singular", "text-domain" ),
    "new_item_name"      => __( "New $singular Name", "text-domain" ),
    "add_or_remove_items"  => __( "Add or remove $plural", "text-domain" ),
    "choose_from_most_used"  => __( "Choose from most used text-domain", "text-domain" ),
    "menu_name"        => __( "$singular", "text-domain" ),
  );

  $args = array(
    'labels'            => $labels,
    'public'            => true,
    'show_in_nav_menus' => true,
    'show_admin_column' => false,
    'hierarchical'      => true,
    'show_tagcloud'     => true,
    'show_ui'           => true,
    'query_var'         => true,
    'rewrite'           => array('slug' => 'product-types'),
    'query_var'         => true,
    'capabilities'      => array(),
  );

  register_taxonomy( 'product-type', array( 'reviews' ), $args );

  $singular1 = "Mood";
  $plural1 = "Moods";

  // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
    "name"                       => _x( "$plural1", "taxonomy general name" ),
    "singular_name"              => _x( "$singular1", "taxonomy singular name" ),
    "search_items"               => __( "Search $plural1" ),
    "popular_items"              => __( "Popular $plural1" ),
    "all_items"                  => __( "All $plural1" ),
    "parent_item"                => null,
    "parent_item_colon"          => null,
    "edit_item"                  => __( "Edit $singular1" ),
    "update_item"                => __( "Update $singular1" ),
    "add_new_item"               => __( "Add New $singular1" ),
    "new_item_name"              => __( "New $singular1 Name" ),
    "separate_items_with_commas" => __( "Separate $plural1 with commas" ),
    "add_or_remove_items"        => __( "Add or remove $plural1" ),
    "choose_from_most_used"      => __( "Choose from the most used $plural1" ),
    "not_found"                  => __( "No $plural1 found." ),
    "menu_name"                  => __( "$plural1" ),
  );

  $args = array(
    'hierarchical'          => false,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'moods' ),
  );

  register_taxonomy( 'mood', array('reviews', 'post'), $args );

  // Price range taxonomy
  $singular2 = "Price range";
  $plural2 = "Price ranges";
  $labels = array(
    "name"              => _x( "$plural2", "taxonomy general name" ),
    "singular_name"     => _x( "$singular2", "taxonomy singular name" ),
    "search_items"      => __( "Search $plural2" ),
    "all_items"         => __( "All $plural2" ),
    "parent_item"       => __( "Parent $singular2" ),
    "parent_item_colon" => __( "Parent $singular2:" ),
    "edit_item"         => __( "Edit $singular2" ),
    "update_item"       => __( "Update $singular2" ),
    "add_new_item"      => __( "Add New $singular2" ),
    "new_item_name"     => __( "New $singular2" ),
    "menu_name"         => __( "$singular2" ),
  );

  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'prices' ),
  );

  register_taxonomy( 'price', array( 'reviews' ), $args );
}

add_action( 'init', 'my_custom_taxonomies' );